Author: Robert Winkler 226 474
Course: Robotic Programming Environments
Ex. title: lab3 - Distributed Communication

Description:

Using the ZeroMQ library, write programs that communicate through the pub/sub
protocol with envelope. Programs should send a complex data structure
using serialization. To do so, use the Google Protocol Buffers library.

# Building a code

```
mkdir build
cd build
cmake ..
make
```

# Running the code

Run the publisher
```
cd build
./publisher
```

Run the subscriber
```
cd build
./subscriber
```

## Result

Subscriber will print the recived message on its output

