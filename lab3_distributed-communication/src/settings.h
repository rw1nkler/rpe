/*
 * Author: Robert Winkler 226 474
 * Contains global settings for the pub-sub communication
 */

#ifndef LAB3_SETTINGS_H
#define LAB3_SETTINGS_H

#define LAB3_PUBLISHER_PORT    "tcp://*:5563"
#define LAB3_SUBSCRIBER_PORT   "tcp://localhost:5563"

#define LAB3_PUB_SUB_CHANNEL   "channel0"

#endif /* LAB3_SETTINGS_H */

