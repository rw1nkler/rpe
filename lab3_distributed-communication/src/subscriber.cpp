/*
 * Author: Robert Winkler 226 474
 * Contains subscriber implementation using ZeroMQ library
 */

#include <iostream>

#include "zhelpers.h"
#include "person.pb.h"
#include "settings.h"

int main (void)
{
  // Create a context (exactly one for a process)
  void *context = zmq_ctx_new();
  // Create a subscriber socket
  void *subscriber = zmq_socket(context, ZMQ_SUB);

  // Connect to a publisher socket.
  // Subscriber is a client
  zmq_connect(subscriber, LAB3_SUBSCRIBER_PORT);
  // Set listening on a proper channel of a socket
  zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE, LAB3_PUB_SUB_CHANNEL, 1);

  // Create a data structure
  lab3::Person data;
  char *channel, *contents;

  while (1) {
    // Receive a "bare" message
    channel = s_recv(subscriber);
    contents = s_recv(subscriber);

    // Covert the "bare" message to a string and initialize the data structure
    std::string str_contents = contents;
    data.ParseFromString(str_contents);

    if (data.IsInitialized()) {
      // Display the content of the data structure
      std::cout << data.DebugString() << std::endl;
    }

    // Dealocate message memory
    free(channel);
    free(contents);
  }

  // Deallocate context and socket memory
  zmq_close(subscriber);
  zmq_ctx_destroy(context);
  return 0;
}

