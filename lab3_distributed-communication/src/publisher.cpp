/*
 * Author: Robert Winkler 226 474
 * Contains publisher implementation using ZeroMQ library
*/

#include <unistd.h>
#include <iostream>
#include <string>

#include "zhelpers.h"
#include "person.pb.h"
#include "settings.h"

void init_data(lab3::Person &person)
{
  person.set_name("Tutorial data");
  person.set_id(123);
  person.set_email("name.surname@gmail.com");

  lab3::Person::PhoneNumber *phone = person.add_phones();
  phone->set_number("123-456-789");
  phone->set_type(lab3::Person::MOBILE);
}

int main (void)
{
  // Create a context (exactly one for a process)
  void *context = zmq_ctx_new();
  // Create a publisher socket
  void *publisher = zmq_socket(context, ZMQ_PUB);
  // We assume that a publisher has a known IP address and act as a server
  zmq_bind(publisher, LAB3_PUBLISHER_PORT);

  // Prepare the data that will be send through the socket
  lab3::Person data;
  init_data(data);
  std::string output;

  while (1) {
    // Serialize data from the data structure to a string
    data.SerializeToString(&output);

    // Send the string in a proper channel
    s_sendmore(publisher, const_cast<char*>(LAB3_PUB_SUB_CHANNEL));
    s_send(publisher, const_cast<char*>(output.c_str()));

    sleep(1);
  }

  // Deallocate and socket memory
  zmq_close(publisher);
  zmq_ctx_destroy(context);
}

