# libzmq installation

## Debian

Install libzmq:
```
git clone --recursive https://github.com/zeromq/libzmq
mkdir libzmq/build
cd libzmq/build
cmake ..
sudo make -j8 install
```

For C++ install cppzmq:
```
git clone --recursive https://github.com/zeromq/cppzmq
mkdir cppzmq/build
cd cppzmq/build
cmake ..
sudo make -j8 install
```

# Usage

## Minimal cmake:

```
cmake_minimum_required(VERSION 3.3)
project(my_project)
find_package(cppzmq)

add_executable(my_executable main.cpp)
target_link_libraries(my_executable cppzmq)
```

